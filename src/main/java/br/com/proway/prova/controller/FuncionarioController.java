package br.com.proway.prova.controller;

import br.com.proway.prova.model.Funcionario;
import br.com.proway.prova.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@RestController
@RequestMapping("funcionario")
public class FuncionarioController {

    @Autowired
    private FuncionarioRepository repository;

    @GetMapping("/by/{id}")
    public Object getFuncionario(@PathVariable("id") long id){
        return repository.findById(id);
    }

    @GetMapping("/all/contratados")
    public List<Object> getAllFuncionarios() {
        return repository.contratados();
    }

    @GetMapping("/all/demitidos")
    public List<Object> getAllExFuncionarios() {
        return repository.demitidos();
    }

    @PostMapping("/contratar")
    public ResponseEntity<Funcionario> contratar(@RequestBody Funcionario funcionario) {
        Funcionario f;
        try {
            f = repository.save(funcionario);
            return new ResponseEntity<>(f, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage() + e.getStackTrace());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/demitir/{id}")
    public void putById(@PathVariable("id") Long id, @RequestBody Funcionario funcionario) {
            funcionario.setId(id);
            funcionario.setFimContrato(LocalDate.now());
            funcionario.setDuracaoContrato(ChronoUnit.YEARS.between(funcionario.getInicioContrato(), LocalDate.now()));
            repository.save(funcionario);
    }

}
