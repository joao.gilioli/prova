package br.com.proway.prova.model;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Data
@Entity
@Table(name = "funcionario")
@EntityListeners(AuditingEntityListener.class)
public class Funcionario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "sobrenome")
    private String sobrenome;

    @Column(name = "funcao")
    private String funcao;

    @Column(name = "salario")
    private Double salario;

    @Column(name = "inicio_contratado")
    private LocalDate inicioContrato;

    @Column(name = "fim_contratado")
    private LocalDate fimContrato;

    @Column(name = "duracao_contrato")
    private Long duracaoContrato;

}