package br.com.proway.prova.repository;

import br.com.proway.prova.model.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

    Object findById(long id);

    List<Funcionario> findAll();

    Funcionario save(Funcionario funcionario);


    @Query("select f.nome from Funcionario f where f.fimContrato is null")
    List<Object> contratados();

    @Query("select f.nome from Funcionario f where f.fimContrato is not null")
    List<Object> demitidos();

}
